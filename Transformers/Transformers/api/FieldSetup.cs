﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    public interface FieldSetup
    {
        void AddUnit(object unit, int pos);
    }
}
