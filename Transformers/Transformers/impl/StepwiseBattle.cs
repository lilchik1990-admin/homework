﻿using System;
using System.Collections.Generic;

namespace Transformers
{
    internal class StepwiseBattle : Battle
    {
        private readonly ICollection<Battler> battlers;
        private readonly FieldSetup fieldSetup;

        private readonly Inventory inventory;
        private Battler winner;

        public StepwiseBattle(FieldSetup fieldSetup, Inventory inventory, params Battler[] battlers)
        {
            var bs = new List<Battler>();
            bs.AddRange(battlers);
            this.battlers = bs.AsReadOnly();
            this.fieldSetup = fieldSetup;
            this.inventory = inventory;
        }

        public void Continue()
        {
            foreach (var b in battlers)
            {
                if (b.IsAlive)
                {
                    b.Act();
                }
            }
        }

        public bool IsFinished()
        {
            return winner != null;
        }

        public void Review()
        {
            int aliveCount = 0;
            foreach (var b in battlers)
            {
                if (b.IsAlive)
                {
                    aliveCount++;
                    winner = b;
                }
            }
            if (aliveCount > 1)
            {
                winner = null;
            }
        }

        public void ShowWinner()
        {
            Console.WriteLine($"{winner.Name} has won the battle!");
        }

        public void Start()
        {
            Console.WriteLine($"Please, choose transformers for {battlers.Count} battlers from list below:");
            var ts = inventory.ListTransformers();
            for (int i = 0; i < ts.Count; i++)
            {
                Console.WriteLine($"{i + 1}. {ts[i].Name}");
            }

            int j = 1;
            foreach (var b in battlers)
            {
                b.Prepare(j, fieldSetup, inventory);
                j++;
            }
        }
    }
}