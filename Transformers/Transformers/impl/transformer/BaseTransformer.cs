﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    class BaseTransformer: Transformer
    {
        private string name;
        private int health;
        private bool isHumanoid = false;
        private readonly Field field;
        private Scanner scanner;
        private Weapon weapon;

        private int armor;

        private Transformer foundEnemy;

        public BaseTransformer(string name, Field field)
        {
            this.name = name;
            this.field = field;
            this.health = 100;
            this.armor = 100;
        }

        public string Name => name;

        public bool IsHumanoid => isHumanoid;

        public int Health => health;

        public int Armor => armor;

        public bool FindEnemy()
        {
            Console.WriteLine($"{Name} scans with {scanner}");
            foundEnemy = scanner.Scan(field);
            return foundEnemy != null;
        }

        public void Fire()
        {
            if (isHumanoid && weapon != null)
            {
                Console.WriteLine($"{Name} fires with {weapon}");
                weapon.Shoot(foundEnemy);
            }
        }

        public virtual void Move(int speed)
        {
            field.MoveUnit(this, speed);
        }

        public void Transform()
        {
            isHumanoid = !isHumanoid;
            Console.WriteLine($"{Name} transformed");
        }

        public void AddScanner(Scanner scanner)
        {
            this.scanner = scanner;
            this.scanner.AttachTo(this);
        }

        public void AddWeapon(Weapon weapon)
        {
            this.weapon = weapon;
        }

        public void Shot(int damage)
        {
            
            if (this.armor > 0)
            {
                Console.WriteLine($"{Name} has received damage in ARMOR {damage}");
                this.armor -= damage;
                return;
            }

           if (this.armor <= 0)
            {
                if (this.armor < 0)
                {
                    Console.WriteLine("Armor is broken");
                    this.health = this.health + this.armor;
                    this.armor = 0;
                }

                Console.WriteLine($"{Name} has received damage {damage}");
                this.health -= damage;
            }
        }
    }
}
