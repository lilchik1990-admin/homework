﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    class Sonar : BaseScanner
    {
        private readonly string model;

        public Sonar(string model, int distance): base(distance)
        {
            this.model = model;
        }

        public override string ToString()
        {
            return $"Sonar {model}";
        }
    }
}
