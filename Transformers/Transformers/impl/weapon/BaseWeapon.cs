﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    public class BaseWeapon : Weapon
    {
        private Random rnd;
        private int count;

        protected BaseWeapon()
        {
            rnd = new Random();
        }

        public void Shoot(Transformer t)
        {
            count++;
            int damage = rnd.Next(5, 25);
            if (count % 3 == 0)
            {
                damage *= 4;
            }
            t.Shot(damage);
        }
    }
}
