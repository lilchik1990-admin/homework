﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Transformers.Tests
{
    public class ScannerTest
    {
        private readonly TestField field;
        private readonly Sonar sonar;

        public ScannerTest()
        {
            field = new TestField();
            sonar = new Sonar("TS11", 15);
        }

        [Fact]
        public void ScannerCanFindEnemy()
        {
            Assert.NotNull(sonar.Scan(field));
        }
    }
}
