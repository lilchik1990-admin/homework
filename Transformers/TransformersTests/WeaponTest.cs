﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Transformers.Tests
{
    public class WeaponTest
    {
        private readonly TestField field;
        private readonly Weapon pistol;

        public WeaponTest()
        {
            field = new TestField();
            field.FindAny(null, null);
            pistol = new Pistol("TP10");
        }

        [Fact]
        public void WeaponDecreasesTransformerHealth()
        {
            Assert.Equal(100, field.FoundEnemy.Health);
            pistol.Shoot(field.FoundEnemy);
            Assert.True(field.FoundEnemy.Health < 100);
        }
    }
}
