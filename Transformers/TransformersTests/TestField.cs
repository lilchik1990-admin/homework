﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transformers;

namespace Transformers.Tests
{
    class TestField : Field
    {
        public int Shift { get; private set; }
        public Transformer FoundEnemy { get; private set; }

        public Transformer FindAny(Transformer owner, Scanner scanner)
        {
            FoundEnemy = new GroundTransformer("GTE", this);
            return FoundEnemy;
        }

        public void MoveUnit(object unit, int shift)
        {
            this.Shift = shift;
        }
    }
}
